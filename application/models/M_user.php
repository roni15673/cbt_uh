<?php
class M_user extends CI_Model{
	public function getAlluser(){
		return $this->db->get_where('user',['role_id' => "1"])->result_array();
	}

	public function getAlluserbyid($id){
		return $this->db->get_where('user',['id' => $id])->row_array();
	}

	public function getlistsoal(){
		return $this->db->get_where('list_soal',['creator' => $this->session->userdata('nis')])->result_array();
	}

	public function getbanksoalbyid($id){
		return $this->db->get_where('bank_soal',['id' => $id])->row_array();
	}

	public function getlistsoal2(){
		$this->db->from('list_soal');
		$this->db->select('bank_soal.kode_soal,list_soal.id,list_soal.guru_mapel,list_soal.mapel,list_soal.deskripsi_ulangan,list_soal.waktu,list_soal.kode_soal','list_soal.creator');
		$this->db->select('count(bank_soal.kode_soal) as jumlah_soal ');
		$this->db->join('bank_soal','list_soal.kode_soal = bank_soal.kode_soal','left')->where('list_soal.creator',$this->session->userdata('nis'));
		$this->db->group_by('list_soal.id');
		return $this->db->get()->result_array();
	}

	public function getlistsoal3(){
		$this->db->from('list_soal');
		$this->db->select('bank_soal.kode_soal,list_soal.id,list_soal.guru_mapel,list_soal.mapel,list_soal.deskripsi_ulangan,list_soal.waktu,list_soal.kode_soal','list_soal.creator');
		$this->db->select('count(bank_soal.kode_soal) as jumlah_soal ');
		$this->db->join('bank_soal','list_soal.kode_soal = bank_soal.kode_soal','left')->where('list_soal.kode_soal',$this->session->userdata('kode'));
		$this->db->group_by('list_soal.id');
		return $this->db->get()->result_array();
	}

	public function getlistsoal4(){
		$this->db->from('list_soal');
		$this->db->select('bank_soal.kode_soal,list_soal.id,list_soal.guru_mapel,list_soal.mapel,list_soal.deskripsi_ulangan,list_soal.waktu,list_soal.kode_soal','list_soal.creator');
		$this->db->select('count(bank_soal.kode_soal) as jumlah_soal ');
		$this->db->join('bank_soal','list_soal.kode_soal = bank_soal.kode_soal','left')->where('list_soal.kode_soal',$this->session->userdata('kode'));
		$this->db->group_by('list_soal.id');
		return $this->db->get()->row_array();
	}

	public function getlistsoalbyid($id){
		return $this->db->get_where('list_soal',['id' => $id])->row_array();
	}

	public function getbanksoal($kode_soal){
		return $this->db->get_where('bank_soal',['kode_soal' => $kode_soal])->result_array();
	}

	public function tambah_user() {
		$data = [
			"nama" => $this->input->post('nama',true),
			"nis" => $this->input->post('nis',true),
			"password" => password_hash($this->input->post('password',true), PASSWORD_DEFAULT),
			"no_abs" => $this->input->post('no_abs',true),
			"kelas" => $this->input->post('kelas',true),
			"tanggal_lahir" => $this->input->post('tanggal_lahir',true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin',true),
			"role_id" => 1
		];
		$this->db->insert('user', $data);
	}

	public function tambah_list_soal() {
		$data = [
			'guru_mapel' => $this->session->userdata('nama'),
			'mapel' => $this->input->post('mapel',true),
			'deskripsi_ulangan' => $this->input->post('deskripsi_ulangan',true),
			'waktu' => $this->input->post('waktu',true),
			'kode_soal' => $this->input->post('kode_soal',true),
			'creator' => $this->session->userdata('nis')
		];
		$this->db->insert('list_soal',$data);
	}

	public function edit_list_soal() {
		$data = [
			'guru_mapel' => $this->session->userdata('nama'),
			'mapel' => $this->input->post('mapel',true),
			'deskripsi_ulangan' => $this->input->post('deskripsi_ulangan',true),
			'waktu' => $this->input->post('waktu',true),
			'kode_soal' => $this->input->post('kode_soal',true)
		];
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('list_soal',$data);	
	}

	public function input_bank_soal($kode_soal){
		$data = [
			'kode_soal' => $kode_soal,
			'no_soal' => $this->input->post('no_soal',true),
			'isi_soal' => $this->input->post('isi_soal',true),
			'opsi_a' => $this->input->post('opsi_a',true),
			'opsi_b' => $this->input->post('opsi_b',true),
			'opsi_c' => $this->input->post('opsi_c',true),
			'opsi_d' => $this->input->post('opsi_d',true),
			'opsi_e' => $this->input->post('opsi_e',true),
			'kunci_jawaban' => $this->input->post('kunci_jawaban',true)
		];
		$this->db->insert('bank_soal',$data);
	}

	public function edit_bank_soal(){
		$data = [
			'isi_soal' => $this->input->post('isi_soal',true),
			'opsi_a' => $this->input->post('opsi_a',true),
			'opsi_b' => $this->input->post('opsi_b',true),
			'opsi_c' => $this->input->post('opsi_c',true),
			'opsi_d' => $this->input->post('opsi_d',true),
			'opsi_e' => $this->input->post('opsi_e',true),
			'kunci_jawaban' => $this->input->post('kunci_jawaban',true)
		];
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('bank_soal',$data);
	}

	public function insert_soal() {
		$data['bank_soal'] = $this->db->get_where('bank_soal',['kode_soal' => $this->session->userdata('kode')])->result_array();
		while($data){
			$data = [
				'nis' => $this->session->userdata('nis'),
				'id_soal' => $data['id'],
				'no_soal' => $data['no'],
				'kode_soal' => $this->session->userdata('kode'),
				'jawaban' => 0

			];
		}
			$this->db->insert('jawaban',$data);
	}

	public function hitung(){
		$this->db->from('jawaban');
		$this->db->select('bank_soal.id,bank_soal.kunci_jawaban,jawaban.id_soal,jawaban.jawaban,jawaban.kode_soal,jawaban.nis');
		$this->db->select('count(*) as jumlah_benar');
		$this->db->join('bank_soal','jawaban.id_soal = bank_soal.id','left')->where('jawaban.jawaban','bank_soal.kunci_jawaban');
		$this->db->group_by('jawaban.nis');
		return $this->db->get()->result_array();
	}

	public function hitung2() {
		$kode = $this->session->userdata('kode');
		return $this->db->query("select nis, count(*) as jumlah_benar from jawaban join bank_soal on jawaban.id_soal = bank_soal.id where jawaban.jawaban = bank_soal.kunci_jawaban and jawaban.kode_soal = '$kode ' group by nis")->result_array();
	}

	public function hitung3() {
		$kode = $this->session->userdata('nis');
		return $this->db->query("select nis, count(*) as jumlah_benar from jawaban join bank_soal on jawaban.id_soal = bank_soal.id where jawaban.jawaban = bank_soal.kunci_jawaban and jawaban.nis = '$kode ' group by nis")->row_array();
	}

}
?>