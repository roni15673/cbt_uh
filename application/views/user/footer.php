<footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b><?php echo $user['nama'];?> |</b> <strong><a href="<?php echo site_url('login/logout');?>">Logout</a></strong>
      </div>
      <strong>Copyright &copy; 2019 <a href="">Developer</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('asset/adminlte');?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('asset/adminlte');?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('asset/adminlte');?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('asset/adminlte');?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte');?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte');?>/dist/js/demo.js"></script>
</body>
</html>
