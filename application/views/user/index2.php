
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h4 class="box-title"></h4>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Mapel</th>
                  <th>Deskripsi</th>
                  <th>Token</th>
                  <th>Waktu</th>
                  <th>Jumlah Soal</th>
                  <th>Guru Mapel</th>
                  <th></th>
                </tr>
                <?php foreach($soal as $u):?>
                <tr>
                  <td>1</td>
                  <td><?php echo $u['mapel'];?></td>
                  <td><?php echo $u['deskripsi_ulangan'];?></td>
                  <td><?php echo $u['kode_soal'];?></td>
                  <td><?php echo $u['waktu'];?></td>
                  <td><?php echo $u['jumlah_soal'];?></td>
                  <td><?php echo $u['guru_mapel'];?></td>
                  <td>
                   <a href="<?php echo site_url('user/soal2/1');?>"><button class="btn btn-primary">Kerjakan <i class="fa  fa-arrow-circle-right"></i></button></a>
                    </td>
                </tr>
              <?php endforeach?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
