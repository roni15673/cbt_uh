
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <div class="callout callout-info">
          <h4>Selamat datang <?php echo $user['nama'];?>!</h4>

          <p>Selamat datang di aplikasi ulangan berbasis computer, silahkan masukan token untuk mulai mengerjakan soal. Apabila belum mendapatkan token silahkan hubungi guru yang bersangkutan atau teknisi sekolah.</p>
        </div>
        <!-- input states -->
        <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Masukan Token</label>
               <form action="<?php echo site_url('user');?>" method="post">
                  <div class="input-group input-group-sm has-success">
                <input type="text" class="form-control" placeholder="enter....." name="kode_soal">
                    <span class="input-group-btn">
                     <button type="submit" class="btn btn-success btn-flat">Go</button>
                    </span>
              </div>
               </form>
              <div class="row">
                <div class="col-sm-12">
                  <small class="text-danger"><?php echo form_error('kode_soal');?></small>
                  <small class="text-danger"><?php echo $this->session->flashdata('pesan');?></small>
                </div>
              </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
