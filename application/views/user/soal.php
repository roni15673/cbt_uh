<style type="text/css">
  li{
    list-style: none;
    margin-bottom: 5px;   
  }
  li button{
    width: 50px;
  }

  .isi-soal{
    margin-bottom: 15px;  
  }

  .box-nomor{
    margin-top: 80px;
  }

  .btn-soal{
    margin-right: 5px;
  }

  .box-soal{
    padding-bottom: 20px;
  }

  .box-body{
    margin-bottom: 10px;
  }

  .btn-opsi{
    border-radius: 50%;
    width: 30px;
    padding: 5px;
    border: 1px solid black;
    margin-right: 8px;
    margin-bottom: 8px;
  }

  .btn-nomor{
    width: 40px;
    margin-right: 5px;
  }
</style>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
    <div class="box box-default box-soal">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $soal['mapel'];?></h3>
          </div>
          <div class="box-body">
           <div class="isi-soal"><b><?php echo $bank_soal2['no_soal'];?></b>. <?php echo $bank_soal2['isi_soal'];?></div>
            <ul>
              <li>
                <form action="<?php echo site_url('user/jawab');?>" method="post">
                <input type="hidden" name="jawaban" value="A">
                <input type="hidden" name="id_soal" value="<?php echo $bank_soal2['id'];?>">
                <input type="hidden" name="no_soal" value="<?php echo $bank_soal2['no_soal'];?>">
                <button type="submit" class="btn btn-default btn-sm btn-opsi"><b>A</b></button> <?php echo $bank_soal2['opsi_a'];?>.
                </form>
              </li>
             <li>
                <form action="<?php echo site_url('user/jawab');?>" method="post">
                <input type="hidden" name="jawaban" value="B">
                <input type="hidden" name="id_soal" value="<?php echo $bank_soal2['id'];?>">
                <input type="hidden" name="no_soal" value="<?php echo $bank_soal2['no_soal'];?>">
                <button type="submit" class="btn btn-default btn-sm btn-opsi"><b>B</b></button> <?php echo $bank_soal2['opsi_b'];?>.
                </form>
              </li>
              <li>
                <form action="<?php echo site_url('user/jawab');?>" method="post">
                <input type="hidden" name="jawaban" value="C">
                <input type="hidden" name="id_soal" value="<?php echo $bank_soal2['id'];?>">
                <input type="hidden" name="no_soal" value="<?php echo $bank_soal2['no_soal'];?>">
                <button type="submit" class="btn btn-default btn-sm btn-opsi"><b>C</b></button> <?php echo $bank_soal2['opsi_c'];?>.
                </form>
              </li>
             <li>
                <form action="<?php echo site_url('user/jawab');?>" method="post">
                <input type="hidden" name="jawaban" value="D">
                <input type="hidden" name="id_soal" value="<?php echo $bank_soal2['id'];?>">
                <input type="hidden" name="no_soal" value="<?php echo $bank_soal2['no_soal'];?>">
                <button type="submit" class="btn btn-default btn-sm btn-opsi"><b>D</b></button> <?php echo $bank_soal2['opsi_d'];?>.
                </form>
              </li>
              <li>
                <form action="<?php echo site_url('user/jawab');?>" method="post">
                <input type="hidden" name="jawaban" value="E">
                <input type="hidden" name="id_soal" value="<?php echo $bank_soal2['id'];?>">
                <input type="hidden" name="no_soal" value="<?php echo $bank_soal2['no_soal'];?>">
                <button type="submit" class="btn btn-default btn-sm btn-opsi"><b>E</b></button> <?php echo $bank_soal2['opsi_e'];?>.
                </form>
              </li>
            </ul><br>
          </div>
          <!-- /.box-body -->
        </div>
      <!-- /.content -->

       <div class="box box-default box-nomor">
          <div class="box-header with-border">
            <p>Nomor yang sudah dikerjakan akan berwarna biru</p>
          </div>
          <div class="box-body">
           <div class="isi-soal"> 
            <?php foreach($bank_soal as $u):?>
              <a href="<?php echo site_url('user/soal2/');?><?php echo $u['no_soal']?>" class="btn btn-default btn-nomor"><?php echo $u['no_soal'];?></a>
          <?php endforeach;?>
           </div>
             <div class="col-12">
               <a href="<?php echo site_url('user/verify');?>" class="btn btn-success pull-right">Hentikan Tes</a>
             </div>
          </div>
          <!-- /.box-body -->
        </div>
      <!-- /.content -->


    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
