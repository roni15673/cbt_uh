
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <div class="callout callout-info">
          <h4>Simpan Ulangan</h4>
          <label>Nama : </label> <?php echo $user['nama'];?><br>
          <label>Kelas : </label> <?php echo $user['kelas'];?><br>
          <label>Mapel : </label> <?php echo $soal['mapel'];?><br>
          <label>Nilai : </label> <?php echo $nilai['jumlah_benar']*5;?><br>
          <p>Terima kasih telah melakukan ulangan harian. Nilai akan di kirimkan ke guru yang bersangkutan.</p>
          <a href="<?php echo site_url('login/logout');?>" class="btn btn-success">Logout</a>
        </div>
              <div class="row">
                <div class="col-sm-12">
                  <small class="text-danger"><?php echo form_error('kode_soal');?></small>
                  <small class="text-danger"><?php echo $this->session->flashdata('pesan');?></small>
                </div>
              </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
