<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $judul;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte');?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte');?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte');?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte');?>/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte');?>/plugins/iCheck/square/blue.css">
  <!-- favicon -->
  <link rel="shortcut icon" href="<?php echo base_url('asset/img/fav.png');?>">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="register-box">

  <div class="register-box-body">
    <p class="login-box-msg">Registrasi</p>

    <form action="" method="post">
      <div class="row">
        <div class="col-md-6">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="nama" placeholder="Nama lengkap" value="<?php echo set_value('nama');?>">
        <small class="text-danger"><?php echo form_error('nama');?></small>
      </div>
      <div class="form-group has-feedback">
        <input type="number" class="form-control" name="nis" placeholder="NIS" value="<?php echo set_value('nis');?>">
         <small class="text-danger"><?php echo form_error('nis');?></small>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo set_value('password');?>">
         <small class="text-danger"><?php echo form_error('password');?></small>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password2" placeholder="Ulangi password" value="<?php echo set_value('password2');?>">
         <small class="text-danger"><?php echo form_error('password2');?></small>
      </div>
        </div><!-- form 1 -->
           <div class="col-md-6">
          <div class="form-group has-feedback">
        <input type="number" class="form-control" name="no_abs" placeholder="No Absen" value="<?php echo set_value('no_abs');?>">
         <small class="text-danger"><?php echo form_error('no_abs');?></small>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="kelas" placeholder="Kelas" value="<?php echo set_value('kelas');?>">
         <small class="text-danger"><?php echo form_error('kelas');?></small>
      </div>
      <div class="form-group has-feedback">
        <input type="date" class="form-control" name="tanggal_lahir" placeholder="tanggal_lahir" value="<?php echo set_value('tanggal_lahir');?>">
         <small class="text-danger"><?php echo form_error('tanggal_lahir');?></small>
      </div>
      <div class="form-group has-feedback">
       <select name="jenis_kelamin" class="form-control" value="<?php echo set_value('jenis_kelamin');?>">
         <option selected="" disabled="">Jenis kelamin</option>
         <?php foreach($jenis_kelamin as $u):?>
          <option><?php echo $u;?></option>
         <?php endforeach;?>
       </select>
        <small class="text-danger"><?php echo form_error('jenis_kelamin');?></small>
      </div>
        </div><!-- form 1 -->
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="<?php echo site_url('login');?>" class="text-center">Login</a>
  </div>
  <!-- /.form-box -->
</div>

<!-- jQuery 3 -->
<script src="<?php echo base_url('asset/adminlte');?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('asset/adminlte');?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('asset/adminlte');?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
s