    <section class="content">
     <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Mapel</label>
                        <input type="hidden" name="id" value="<?php echo $soal['id'];?>">
                        <input type="text" class="form-control" id="mapel" name="mapel" value="<?php echo $soal['mapel'];?>" placeholder="masukan mapel">
                        <small class="text-danger"><?php echo form_error('mapel');?></small>
                      </div>
                      <div class="form-group">
                        <label>Deskripsi Ulangan</label>
                        <input type="text" class="form-control" id="deskripsi_ulangan" name="deskripsi_ulangan" value="<?php echo $soal['deskripsi_ulangan'];?>" placeholder="masukan dekskripsi">
                        <small class="text-danger"><?php echo form_error('deskripsi_ulangan');?></small>
                      </div>
                  </div><!-- formkiri -->
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Waktu</label>
                        <input type="text" class="form-control" id="waktu" name="waktu" value="<?php echo $soal['waktu'];?>" placeholder="Menit">
                        <small class="text-danger"><?php echo form_error('waktu');?></small>
                      </div>
                      <div class="form-group">
                        <label>Kode Ulangan</label>
                        <input type="kode_soal" class="form-control" id="kode_soal" readonly="" name="kode_soal" value="<?php echo $soal['kode_soal'];?>" placeholder="masukan kode ulangan">
                        <small class="text-danger"><?php echo form_error('kode_soal');?></small>
                      </div>
                  </div><!-- formkanan -->
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?php echo site_url('admin/list_soal');?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        

     </section>
  </div>
 