    <section class="content">
     <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                        <label>Soal</label>
                        <input type="hidden" name="id" value="<?php echo $soal['id'];?>">
                         <input type="hidden" name="kode_soal" value="<?php echo $soal['kode_soal'];?>">
                        <input type="text" class="form-control" id="isi_soal" name="isi_soal" value="<?php echo $soal['isi_soal'];?>" placeholder="">
                        <small class="text-danger"><?php echo form_error('isi_soal');?></small>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label> A</label>
                        <input type="text" class="form-control" id="opsi_a" name="opsi_a" value="<?php echo $soal['opsi_a'];?>" placeholder="masukan opsi A">
                        <small class="text-danger"><?php echo form_error('opsi_a');?></small>
                      </div>
                      <div class="form-group">
                        <label> B</label>
                        <input type="text" class="form-control" id="opsi_b" name="opsi_b" value="<?php echo $soal['opsi_b'];?>" placeholder="masukan opsi B">
                        <small class="text-danger"><?php echo form_error('opsi_b');?></small>
                      </div>
                      <div class="form-group">
                        <label> C</label>
                        <input type="text" class="form-control" id="opsi_c" name="opsi_c" value="<?php echo $soal['opsi_c'];?>" placeholder="masukan opsi C">
                        <small class="text-danger"><?php echo form_error('opsi_c');?></small>
                      </div>
                  </div><!-- formki -->
                  <div class="col-md-6">
                      <div class="form-group">
                        <label> D</label>
                        <input type="text" class="form-control" id="opsi_d" name="opsi_d" value="<?php echo $soal['opsi_d'];?>" placeholder="masukan opsi D">
                        <small class="text-danger"><?php echo form_error('opsi_d');?></small>
                      </div>
                      <div class="form-group">
                        <label> E</label>
                        <input type="text" class="form-control" id="opsi_e" name="opsi_e" value="<?php echo $soal['opsi_e'];?>" placeholder="masukan opsi E">
                        <small class="text-danger"><?php echo form_error('opsi_e');?></small>
                      </div>
                      <div class="form-group">
                        <label>Kunci Jawaban</label>
                        <select class="form-control" name="kunci_jawaban">
                          <option>A</option>
                          <option>B</option>
                          <option>C</option>
                          <option>D</option>
                          <option>E</option>
                        </select>
                        <small class="text-danger"><?php echo form_error('kunci_jawaban');?></small>
                      </div>
                  </div><!-- formkanan -->
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?php echo site_url('admin/list_soal');?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        

     </section>
  </div>
 