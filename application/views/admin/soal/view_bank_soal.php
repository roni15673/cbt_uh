<style type="text/css">
  .tombol-tabel{
    width: 45px;
    margin-bottom: 5px; 
  }
</style>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-xs-12">
           <?php 
              $url = $_SERVER['REQUEST_URI'];
              $ex = explode("/", $url);

             ?>
              <a href="<?php echo site_url('admin/list_soal');?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Kembali</a>
          <a href="<?php echo site_url('admin/input_bank_soal/');?><?php echo $ex[5]; ?>" class="btn btn-primary mb-3">Tambah Soal <i class="fa fa-plus"></i></a>
          <div class="box">
            <div class="box-header">
              <h4 class="box-title"></h4>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No Soal</th>
                  <th>Soal</th>
                  <th>Opsi A</th>
                  <th>Opsi B</th>
                  <th>Opsi C</th>
                  <th>Opsi D</th>
                  <th>Opsi E</th>
                  <th>Kunci Jawaban</th>
                  <th>Action</th>
                </tr>
                <?php
                $no = 1; 
                foreach($bank_soal as $soal):?>
                <tr>
                  <td><?php echo $soal['no_soal'];?></td>
                  <td><?php echo $soal['isi_soal'];?></td>
                  <td><?php echo $soal['opsi_a'];?></td>
                  <td><?php echo $soal['opsi_b'];?></td>
                  <td><?php echo $soal['opsi_c'];?></td>
                  <td><?php echo $soal['opsi_d'];?></td>
                  <td><?php echo $soal['opsi_e'];?></td>
                  <td><?php echo $soal['kunci_jawaban'];?></td>
                  <td>
                    <a href="<?php echo site_url('admin/hapus_bank_soal/');?><?php echo $soal['id'];?>" class="btn btn-danger tombol-tabel"><i class="fa fa-trash"></i></a>
                    <a href="<?php echo site_url('admin/edit_bank_soal/');?><?php echo $soal['id'];?>" class="btn btn-success tombol-tabel"><i class="fa fa-edit"></i></a>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
            </div>
            <!-- /.box-body -->
             <div class="terima-kasih" id="terimakasih" data-isi="<?php echo $this->session->flashdata('pesan');?>"></div>
          </div>
          <!-- /.box -->
        </div>
      </div>

     </section>
  </div>
  <!-- /.content-wrapper -->
 