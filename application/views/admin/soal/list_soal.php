

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-xs-12">
          <a href="<?php echo site_url('admin/tambah_list_soal');?>" class="btn btn-primary mb-3">Tambah Ulangan <i class="fa fa-plus"></i></a>
          <div class="box">
            <div class="box-header">
              <h4 class="box-title"></h4>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Mapel</th>
                  <th>Deskripsi</th>
                  <th>Kode Soal</th>
                  <th>Waktu</th>
                  <th>Jumlah Soal</th>
                  <th>Guru Mapel</th>
                  <th>Action</th>
                </tr>
                <?php 
                $no = 1;
                foreach($soal as $soal):?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $soal['mapel'];?></td>
                  <td><?php echo $soal['deskripsi_ulangan'];?></td>
                  <td><?php echo $soal['kode_soal'];?></td>
                  <td><?php echo $soal['waktu'];?></td>
                  <td><?php echo $soal['jumlah_soal'];?></td>
                  <td><?php echo $soal['guru_mapel'];?></td>
                  <td>
                    <a href="<?php echo site_url('admin/hapus_list_soal/');?><?php echo $soal['kode_soal'];?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    <a href="<?php echo site_url('admin/edit_list_soal/');?><?php echo $soal['id'];?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('admin/input_bank_soal/');?><?php echo $soal['kode_soal'];?>" class="btn btn-primary">Input Soal <i class="fa fa-share"></i></a>
                     <a href="<?php echo site_url('admin/view_bank_soal/');?><?php echo $soal['kode_soal'];?>" class="btn btn-warning">View Soal <i class="fa fa-eye"></i></a>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
            </div>
            <!-- /.box-body -->
             <div class="terima-kasih" id="terimakasih" data-isi="<?php echo $this->session->flashdata('pesan');?>"></div>
          </div>
          <!-- /.box -->
        </div>
      </div>

     </section>
  </div>
  <!-- /.content-wrapper -->
 