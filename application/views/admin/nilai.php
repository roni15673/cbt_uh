</style>
<style type="text/css">
  .pesan-error{
    margin-left: 20px;
  }
</style>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
     <div class="callout callout-info">
          <h4>Selamat datang <?php echo $user['nama'];?>.</h4>

          <p>Selamat datang di aplikasi ulangan berbasis computer, ini adalaha halaman untuk admin atau guru. Silahkan masukan token untuk melihat nilai. Setelah siswa mengerjakan semua soal soal ulang nilai akan otomatis masuk ke halaman admin anda.</p>
        </div>
          <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Masukan Token</label>
               <form action="" method="post">
                  <div class="input-group input-group-sm has-success">
                <input type="text" class="form-control" placeholder="enter....." name="kode_soal">
                    <span class="input-group-btn">
                     <button type="submit" class="btn btn-success btn-flat">Go</button>
                    </span>
              </div>
              <div class="row">
                <div class="col-12">
                  <small class="text-danger pesan-error"><?php echo form_error('kode_soal');?></small>
                  <small class="text-danger"><?php echo $this->session->flashdata('pesan');?></small>
                </div>
              </div>
               </form>
     </section>
    </div>
  </div>
  <!-- /.content-wrapper -->
 