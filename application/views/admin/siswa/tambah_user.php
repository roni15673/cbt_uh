<style type="text/css">
  .kotak1{
    height: 500px;
  }
</style>
<!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Siswa</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo site_url('admin/tambahsiswa');?>" method="POST">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?php echo set_value('nama');?>" placeholder="masukan nama">
                        <small class="text-danger"><?php echo form_error('nama');?></small>
                      </div>
                      <div class="form-group">
                        <label>NIS</label>
                        <input type="text" class="form-control" id="nis" name="nis" value="<?php echo set_value('nis');?>" placeholder="masukan nis">
                        <small class="text-danger"><?php echo form_error('nis');?></small>
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password');?>" placeholder="ulangi nis">
                        <small class="text-danger"><?php echo form_error('password');?></small>
                      </div>
                      <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" class="form-control" id="password2" name="password2" value="<?php echo set_value('password2');?>" placeholder="ulangi nis">
                        <small class="text-danger"><?php echo form_error('password2');?></small>
                      </div>
                  </div><!-- formkiri -->
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>No Absen Siswa</label>
                        <input type="number" class="form-control" id="no_absen" name="no_abs" value="<?php echo set_value('no_abs');?>" placeholder="no absen">
                        <small class="text-danger"><?php echo form_error('no_absen');?></small>
                      </div>
                      <div class="form-group">
                        <label>Kelas</label>
                        <input type="kelas" class="form-control" id="kelas" name="kelas" value="<?php echo set_value('kelas');?>" placeholder="masukan kelas">
                        <small class="text-danger"><?php echo form_error('kelas');?></small>
                      </div>
                      <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="<?php echo set_value('tanggal_lahir');?>" placeholder="masukan tanggal lahir">
                        <small class="text-danger"><?php echo form_error('tanggal_lahir');?></small>
                      </div>
                      <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <select class="form-control" name="jenis_kelamin">
                        <option selected="" disabled="">Pilih jenis kelamin</option>
                        <?php foreach($jenis_kelamin as $u):?>
                        <option><?php echo $u;?></option>
                      <?php endforeach;?>
                      </select>
                      <small class="text-danger"><?php echo form_error('jenis_kelamin');?></small>
                </div>
                  </div><!-- formkanan -->
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?php echo site_url('admin/siswa');?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
     </section>
  </div>
  <!-- /.content-wrapper -->