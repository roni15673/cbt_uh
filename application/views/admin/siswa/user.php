<!-- Main content -->
    <section class="content">
    <a href="<?php echo site_url('admin/tambahsiswa');?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
    <div class="box">
            <div class="box-header mt-3">
              <h3 class="box-title">Data Siswa</h3>
              <div class="terima-kasih" id="terimakasih" data-isi="<?php echo $this->session->flashdata('pesan');?>"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kelas</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Action</th>
                </tr>
                          </thead>
                <?php
                $no = 1;
                foreach($siswa as $u) :
                ?>
                <tbody>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $u['nis'];?></td>
                  <td><?php echo $u['nama'];?></td>
                  <td> <?php echo $u['kelas'];?></td>
                  <td>
                    <a href="<?php echo site_url('admin/hapussiswa/');?><?php echo $u['id'];?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    <a href="<?php echo site_url('admin/editsiswa/');?><?php echo $u['id'];?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('admin/detail_siswa');?><?php echo $u['id'];?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                </tfoot>
                <?php endforeach;?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
     </section>
  </div>
  <!-- /.content-wrapper -->