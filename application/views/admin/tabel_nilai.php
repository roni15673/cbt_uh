<!-- Main content -->
    <section class="content">
    <a href="<?php echo site_url('admin/nilai');?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Kembali</a>
    <a href="<?php echo site_url('admin/excelnilai');?>" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Excel</a>
    <a href="<?php echo site_url('admin/pdfnilai');?>" target="_blank" class="btn btn-danger"><i class="fa fa-file-excel-o"></i> PDF</a>
    <div class="box">
            <div class="box-header mt-3">
              <h3 class="box-title">Data Siswa</h3>
              <div class="terima-kasih" id="terimakasih" data-isi="<?php echo $this->session->flashdata('pesan');?>"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nis</th>
                  <th>Benar</th>
                  <th>Nilai</th>
                </tr>
                          </thead>
                <?php
                $no = 1;
                foreach($hitung as $u) :
                ?>
                <tbody>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $u['nis'];?></td>
                  <td><?php echo $u['jumlah_benar'];?></td>
                  <th><?php echo $u['jumlah_benar']*5;?></th>
                </tr>
                </tfoot>
                <?php endforeach;?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
     </section>
  </div>
  <!-- /.content-wrapper -->