<?php
class Admin extends CI_Controller{
	public function __construct(){
		parent:: __construct();
		$this->load->model('m_user');
	}

	public function index(){
		$data['judul'] = 'Home | Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();

		$this->load->view('templates/header',$data);
		$this->load->view('admin/home');
		$this->load->view('templates/footer');
	}

	public function siswa(){
		$data['judul'] = 'Daftar siswa | Computer Bassed test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['siswa'] = $this->m_user->getAlluser();

		$this->load->view('templates/header',$data);
		$this->load->view('admin/siswa/user');
		$this->load->view('templates/footer');
	}

	public function detail_siswa($id){
		$data['judul'] = 'Panel Admin';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['siswa'] = $this->m_user->getAlluser();
		$data['siswa2'] = $this->m_user->getAlluserbyid($id);

		$this->load->view('templates/header',$data);
		$this->load->view('admin/user');
		$this->load->view('templates/footer');
	}

	public function tambahsiswa() {
		$data['judul'] = 'Tambah Siswa | Computer Bassed Test';
		$data['jenis_kelamin'] = ["Laki-laki","Perempuan"];
		$data['user'] = $this->db->get_where('user',['nis' => $this->session->userdata('nis')])->row_array();

		$this->form_validation->set_rules('nama', 'Nama', 'required',['required'=> 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('nis', 'NIS', 'required|trim|is_unique[user.nis]',['is_unique'=>'Nis sudah terdaftar','required'=> 'Nis tidak boleh kosong']);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[password2]',['matches'=>'password tidak sama','required'=> 'Password tidak boleh kosong']);
		$this->form_validation->set_rules('password2', 'Konfirmasi Password', 'required|trim|min_length[3]|matches[password]',['matches'=>'Konfirmasi password tidak sama','required'=> 'Konfirmasi password tidak boleh kosong']);
		$this->form_validation->set_rules('no_abs', 'Nomor Absen', 'required',['required'=> 'Nomor absen tidak boleh kosong']);
		$this->form_validation->set_rules('kelas', 'Kelas', 'required',['required'=> 'Kelas tidak boleh kosong']);
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal lahir', 'required',['required'=> 'Tanggal lahir tidak boleh kosong']);
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required',['required'=> 'Jenis kelamin tidak boleh kosong']);
		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/siswa/tambah_user');
		$this->load->view('templates/footer');
		}else{
			$this->m_user->tambah_user();
			$this->session->set_flashdata('pesan','Ditambahkan');
			redirect('admin/siswa');
		}
	}

	public function editsiswa($id) {
		$data['judul'] = 'Panel Admin';
		$data['jenis_kelamin'] = ["Laki-laki","Perempuan"];
		$data['user'] = $this->db->get_where('user',['nis' => $this->session->userdata('nis')])->row_array();
		$data['siswa'] = $this->m_user->getAlluserbyid($id);

		$this->form_validation->set_rules('nama', 'Nama', 'required',['required'=> 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('nis', 'NIS', 'required|trim|is_unique[user.nis]',['is_unique'=>'Nis sudah terdaftar','required'=> 'Nis tidak boleh kosong']);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[password2]',['matches'=>'password tidak sama','required'=> 'Password tidak boleh kosong']);
		$this->form_validation->set_rules('password2', 'Konfirmasi Password', 'required|trim|min_length[3]|matches[password]',['matches'=>'Konfirmasi password tidak sama','required'=> 'Konfirmasi password tidak boleh kosong']);
		$this->form_validation->set_rules('no_abs', 'Nomor Absen', 'required',['required'=> 'Nomor absen tidak boleh kosong']);
		$this->form_validation->set_rules('kelas', 'Kelas', 'required',['required'=> 'Kelas tidak boleh kosong']);
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal lahir', 'required',['required'=> 'Tanggal lahir tidak boleh kosong']);
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required',['required'=> 'Jenis kelamin tidak boleh kosong']);
		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/siswa/edit_user');
		$this->load->view('templates/footer');
		}else{
			$this->m_user->tambah_user();
			$this->session->set_flashdata('pesan','Diedit');
			redirect('admin/siswa');
		}
	}

	public function hapussiswa($id) {
		$this->db->delete('user', ['id' => $id]);
		$this->session->set_flashdata('pesan','Di Hapus');
		redirect('admin/siswa');
	}

	public function list_soal(){
		$data['judul'] = 'Panel Admin';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->m_user->getlistsoal2();

		$this->load->view('templates/header',$data);
		$this->load->view('admin/soal/list_soal');
		$this->load->view('templates/footer');
	}

	public function hapus_list_soal($id){
		$this->db->delete('list_soal',['kode_soal' => $id]);
		$this->db->delete('bank_soal',['kode_soal' => $id]);
		$this->session->set_flashdata('pesan','Dihapus');
		redirect('admin/list_soal');
	}

	public function tambah_list_soal() {
		$data['judul'] = 'Panel Admin';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();

		$this->form_validation->set_rules('mapel', 'Mapel', 'required',['required'=> 'tidak boleh kosong']);
		$this->form_validation->set_rules('deskripsi_ulangan', 'Deskripsi Ulangan', 'required',['required'=> 'tidak boleh kosong']);
		$this->form_validation->set_rules('waktu', 'Waktu', 'required',['required'=> 'tidak boleh kosong']);
		$this->form_validation->set_rules('kode_soal', 'Kode Ulangan', 'required|is_unique[list_soal.kode_soal]',['required'=> 'tidak boleh kosong','is_unique'=> 'Kode soal sudah di gunakan']);

		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/soal/tambah_list_soal');
		$this->load->view('templates/footer');
		}else{
			$this->m_user->tambah_list_soal();
			$this->session->set_flashdata('pesan','Ditambahkan');
			redirect('admin/list_soal');
		}
	}

	public function edit_list_soal($id) {
		$data['judul'] = 'Panel Admin';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->m_user->getlistsoalbyid($id);

		$this->form_validation->set_rules('mapel', 'Mapel', 'required',['required'=> 'tidak boleh kosong']);
		$this->form_validation->set_rules('deskripsi_ulangan', 'Deskripsi Ulangan', 'required',['required'=> 'tidak boleh kosong']);
		$this->form_validation->set_rules('waktu', 'Waktu', 'required',['required'=> 'tidak boleh kosong']);
		$this->form_validation->set_rules('kode_soal', 'Kode Ulangan', 'required',['required'=> 'tidak boleh kosong']);

		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/soal/edit_list_soal');
		$this->load->view('templates/footer');
		}else{
			$this->m_user->edit_list_soal();
			$this->session->set_flashdata('pesan','Diedit');
			redirect('admin/list_soal');
		}
	}

	public function input_bank_soal($kode_soal) {
		$data['judul'] = 'Input Bank Soal';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();

		$this->form_validation->set_rules('no_soal','Nomor','required');
		$this->form_validation->set_rules('isi_soal','Soal','required');
		$this->form_validation->set_rules('opsi_a','Opsi A','required');
		$this->form_validation->set_rules('opsi_b','Opsi B','required');
		$this->form_validation->set_rules('opsi_c','Opsi C','required');
		$this->form_validation->set_rules('opsi_d','Opsi D','required');
		$this->form_validation->set_rules('opsi_e','Opsi E','required');
		$this->form_validation->set_rules('kunci_jawaban','Kunci Jawaban','required');

		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/soal/input_bank_soal');
		$this->load->view('templates/footer');
		}else{
			$this->m_user->input_bank_soal($kode_soal);
			$this->session->set_flashdata('pesan','Ditambahkan');
			redirect('admin/view_bank_soal/'.$kode_soal);
		}
	}

	public function edit_bank_soal($id) {
		$data['judul'] = 'Input Bank Soal';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->m_user->getbanksoalbyid($id);
		$kode = $this->input->post('kode_soal');

		$this->form_validation->set_rules('isi_soal','Soal','required');
		$this->form_validation->set_rules('opsi_a','Opsi A','required');
		$this->form_validation->set_rules('opsi_b','Opsi B','required');
		$this->form_validation->set_rules('opsi_c','Opsi C','required');
		$this->form_validation->set_rules('opsi_d','Opsi D','required');
		$this->form_validation->set_rules('opsi_e','Opsi E','required');
		$this->form_validation->set_rules('kunci_jawaban','Kunci Jawaban','required');

		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/soal/edit_bank_soal');
		$this->load->view('templates/footer');
		}else{
			$this->m_user->edit_bank_soal();
			$this->session->set_flashdata('pesan','Diedit');
			redirect('admin/view_bank_soal/'.$kode);
		}

	}
	public function hapus_bank_soal($id){
		$kode = $this->m_user->getbanksoalbyid($id);
		$this->db->delete('bank_soal',['id' => $id]);
		$this->session->set_flashdata('pesan','Dihapus');
		redirect('admin/view_bank_soal/'.$kode['kode_soal']);
	}


	public function view_bank_soal($kode_soal){
		$data['judul'] = 'View Soal';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['bank_soal'] = $this->m_user->getbanksoal($kode_soal);

		$this->load->view('templates/header',$data);
		$this->load->view('admin/soal/view_bank_soal');
		$this->load->view('templates/footer');
	}

	public function nilai(){
		$data['judul'] = 'Nilai | Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();

		$this->form_validation->set_rules('kode_soal','Kode Soal','required',['required' => 'Kode soal harus di isi']);
		if($this->form_validation->run() == false){

		$this->load->view('templates/header',$data);
		$this->load->view('admin/nilai');
		$this->load->view('templates/footer');
		}else{
			$this->_input_kode();
		}
	}

	private function _input_kode(){
		$kode = $this->input->post('kode_soal');
		$cek = $this->db->get_where('list_soal',['kode_soal' => $kode])->row_array();

		if($cek){
			$data = [
				'kode' => $cek['kode_soal']
			];
			$this->session->set_userdata($data);
			redirect('admin/tabel_nilai');
		}else{
			$this->session->set_flashdata('pesan','Kode salah');
			redirect('admin/nilai');
		}
	}

	public function tabel_nilai() {
		$data['judul'] = 'Nilai | Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->db->get_where('list_soal',['kode_soal' => $this->session->userdata('kode')])->row_array();
		$data['hitung'] = $this->m_user->hitung2();

		$this->load->view('templates/header',$data);
		$this->load->view('admin/tabel_nilai');
		$this->load->view('templates/footer');
	}

	public function excelnilai() {
		$data['judul'] = 'Nilai | Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->db->get_where('list_soal',['kode_soal' => $this->session->userdata('kode')])->row_array();
		$data['hitung'] = $this->m_user->hitung2();

		$this->load->view('admin/excelnilai.php',$data);
	}

	public function pdfnilai() {
		$data['judul'] = 'Nilai | Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->db->get_where('list_soal',['kode_soal' => $this->session->userdata('kode')])->row_array();
		$data['hitung'] = $this->m_user->hitung2();

		$this->load->view('admin/pdfnilai.php',$data);
	}

}
?>