<?php
class User extends CI_Controller{
	public function __construct(){
		parent:: __construct();
		$this->load->model('m_user');
	}

	public function index(){
		$data['title'] = 'Home | Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();

		$this->form_validation->set_rules('kode_soal','Kode Soal','required|trim',['required' => 'Kode harus di isi!.']);
		if($this->form_validation->run() == false){

		$this->load->view('user/header',$data);
		$this->load->view('user/index');
		$this->load->view('user/footer');
		}else{
			$this->_input_kode();
		}
	}

	public function index2() {
		$data['title'] = 'Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->m_user->getlistsoal3();

		$this->load->view('user/header',$data);
		$this->load->view('user/index2');
		$this->load->view('user/footer');
	}

	public function soal() {
		$data['title'] = 'Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->db->get_where('list_soal',['kode_soal' => $this->session->userdata('kode')])->row_array();
		$data['bank_soal'] = $this->db->get_where('bank_soal',['kode_soal' => $this->session->userdata('kode')])->result_array();

		$this->load->view('user/header',$data);
		$this->load->view('user/soal');
		$this->load->view('user/footer');
	}

	public function soal2($id) {
		$data['title'] = 'Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->db->get_where('list_soal',['kode_soal' => $this->session->userdata('kode')])->row_array();
		$data['bank_soal'] = $this->db->get_where('bank_soal',['kode_soal' => $this->session->userdata('kode')])->result_array();
		$data['bank_soal2'] = $this->db->get_where('bank_soal',['no_soal' => $id, 'kode_soal' => $this->session->userdata('kode')])->row_array();
		$data['cek'] = $this->db->get_where('jawaban',['nis'=> $this->session->userdata('nis')])->row_array();

		$this->load->view('user/header',$data);
		$this->load->view('user/soal');
		$this->load->view('user/footer');
	}

	private function _input_kode(){
		$kode = $this->input->post('kode_soal');
		$cek = $this->db->get_where('list_soal',['kode_soal' => $kode])->row_array();

		if($cek){
			$data = [
				'kode' => $cek['kode_soal']
			];
			$this->session->set_userdata($data);
			redirect('user/index2');
		}else{
			$this->session->set_flashdata('pesan','Kode salah');
			redirect('user');
		}
	}

	public function jawab() {
		$soal = $this->db->get_where('jawaban',['id_soal' => $this->input->post('id_soal'), 'nis' => $this->session->userdata('nis')])->row_array();
		$data = [
				'nis' => $this->session->userdata('nis'),
				'id_soal' => $this->input->post('id_soal'),
				'no_soal' => $this->input->post('no_soal'),
				'kode_soal' => $this->session->userdata('kode'),
				'jawaban' => $this->input->post('jawaban')
				];
		if($soal){
			$this->db->where('id_soal',$this->input->post('id_soal'));
			$this->db->where('nis',$this->session->userdata('nis'));
			$this->db->update('jawaban',$data);
		}else{
			$this->db->insert('jawaban',$data);		
		}
		redirect('user/soal2/'.$data['no_soal']);
	}

	public function verify() {
		$data['title'] = 'Computer Bassed Test';
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$data['soal'] = $this->m_user->getlistsoal4();
		$data['nilai'] = $this->m_user->hitung3();

		$this->load->view('user/header',$data);
		$this->load->view('user/verify');
		$this->load->view('user/footer');
	}

}
?>