<?php
class Login extends CI_Controller{
	public function __construct(){
		parent:: __construct();
		$this->load->model('m_user');
	}

	public function index(){
		$data['judul'] = "Login | Computer Bassed Test";
		$data['user'] = $this->db->get_where('user',['nis'=> $this->session->userdata('nis')])->row_array();
		$this->form_validation->set_rules('nis','NIS','trim|required',['required' => 'NIS harus di isi!.','matches' => 'NIS harus sama dengan password!.']);
		$this->form_validation->set_rules('password','NIS','trim|required',['required' => 'Password harus di isi!.','matches' => 'Password harus sama dengan NIS!.']);
		if($this->form_validation->run() == false){
		$this->load->view('login/v_login',$data);
		}else{
			$this->_login();
		}
	}

	private function _login(){
		$nis = $this->input->post('nis');
		$password = $this->input->post('password');
		$user = $this->db->get_where('user',['nis' => $nis])->row_array();

		// jika user ada
		if($user){
			if(password_verify($password, $user['password'])){
				$data = [
					'nis' => $user['nis'],
					'nama' => $user['nama'],
					'role_id' => $user['role_id']
				];
				$this->session->set_userdata($data);
				if($user['role_id'] == 1){
					redirect('user');
				}else{
					redirect('admin');
				}
			}else{
				$this->session->set_flashdata('message', ' <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i>Password salah.</h4>
              </div>');
			redirect('login');
			}

		}else{
			$this->session->set_flashdata('message', ' <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i>NIS tidak terdaftar.</h4>
              </div>');
			redirect('login');
		}
	}

	public function logout(){
		$this->session->unset_userdata('nis');
		$this->session->unset_userdata('role_id');
		redirect('login');
	}

	public function register(){
		$data['judul'] = 'Register | Compter Bassed Test';
		$data['jenis_kelamin'] = ["Laki-laki","Perempuan"];

		$this->form_validation->set_rules('nama', 'Nama', 'required',['required'=> 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('nis', 'NIS', 'required|trim|is_unique[user.nis]',['is_unique'=>'Nis sudah terdaftar','required'=> 'Nis tidak boleh kosong']);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[password2]',['matches'=>'password tidak sama','required'=> 'Password tidak boleh kosong']);
		$this->form_validation->set_rules('password2', 'Konfirmasi Password', 'required|trim|min_length[3]|matches[password]',['matches'=>'Konfirmasi password tidak sama','required'=> 'Konfirmasi password tidak boleh kosong']);
		$this->form_validation->set_rules('no_abs', 'Nomor Absen', 'required',['required'=> 'Nomor absen tidak boleh kosong']);
		$this->form_validation->set_rules('kelas', 'Kelas', 'required',['required'=> 'Kelas tidak boleh kosong']);
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal lahir', 'required',['required'=> 'Tanggal lahir tidak boleh kosong']);
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required',['required'=> 'Jenis kelamin tidak boleh kosong']);
		if($this->form_validation->run() == false){

		$this->load->view('login/register',$data);
		}else{
			$this->m_user->tambah_user();
			$this->session->set_flashdata('message', ' <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Registrasi Berhasil.</h4>
              </div>');
			redirect('login');
		}
	}
}
?>