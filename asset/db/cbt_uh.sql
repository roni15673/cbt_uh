-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 25, 2019 at 10:27 AM
-- Server version: 5.7.26-0ubuntu0.18.10.1
-- PHP Version: 7.3.7-1+ubuntu18.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cbt_uh`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_soal`
--

CREATE TABLE `bank_soal` (
  `id` int(11) NOT NULL,
  `no_soal` int(11) NOT NULL,
  `isi_soal` text NOT NULL,
  `opsi_a` text NOT NULL,
  `opsi_b` text NOT NULL,
  `opsi_c` text NOT NULL,
  `opsi_d` text NOT NULL,
  `opsi_e` text NOT NULL,
  `kunci_jawaban` varchar(255) NOT NULL,
  `kode_soal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_soal`
--

INSERT INTO `bank_soal` (`id`, `no_soal`, `isi_soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `kunci_jawaban`, `kode_soal`) VALUES
(30, 1, ' Sejarah menjadi suatu pengetahuan yang penting dalam kehidupan suatu bangsa atau suatu negara. Melalui pelajaran sejarah akan mendapat gambaran tentang kehidupan masyarakat di masa lampau atau mengetahui peristiwa-peristiwa yang terjadi di masa lampau.  Dari deskripsi tersebut menunjukkan bahwa peristiwa sejarah merupakan suatu peristiwa penting, karena ....', 'peristiwanya tidak dapat berubah oleh perkembangan jaman', 'peristiwa yang terjadi tetap dikenang sepanjang masa', 'dalam kehidupan peristiwanya hanya terjadi satu kali saja', 'mempunyai arti dalam menentukan kehidupan orang banyak', 'peristiwa yang telah terjadi tidak mungkin terulang kembali', 'D', '89jhu'),
(31, 2, 'Saat kita mengunjungi museum, maka kita melihat banyak sumber sejarah seperti tengkorak manusia, kapak batu, replika kubur batu, replika prasasti, dan masih banyak barang peninggalan budaya masa lalu. Peninggalan-peninggalan tersebut termasuk dalam sumber ....', ' lisan.', ' tertulis.', 'benda.', 'primer.', 'sekunder. ', 'C', '89jhu'),
(32, 3, 'Ketika memasuki museum Ronggowarsito Jawa Tengah, kita seolah-olah memasuki masa lampau. Kita merasakan suasana yang berlainan dengan suasana sekarang. Penjelasan pemandu memperkuat suasana masa lain tersebut. Kita rasakan keindahan dan kejayaan masa lain karena yang ditampilkan di museum adalah hasil peradaban masyarakat dimasa lampau.  Teks tersebut menunjukkan salah satu kegunaan sejarah yaitu fungsi ....', ' edukatif (memberi pelajaran)', 'inspiratif (memberi ilham/penunjuk)', 'instruktif (membantu pengajaran keterampilan)', 'rekreatif (memberi kesenangan dan rasa estetis)', 'imajinatif (menggunakan imajinasi dalam belajar)', 'D', '89jhu'),
(33, 4, 'Kronologi berasal dari bahasa Yunani yaitu chronos berati waktu dan logos berari ilmu atau pengetahuan secara harafiah berarti ilmu tentang waktu. Konsep kronologis sangat diperlukan dalam mempelajari ilmu sejarah.  Konsep ini bertujuan untuk....', 'menyeleksi berbagai peristiwa', ' mengklarifikasi berbagai perisriwa', ' membuat urutan peristiwa berdasarkan tahun', ' mengungkapkan berbagai peristiwa', 'membuat pedoman peristiwa penting', 'C', '89jhu'),
(34, 1, 'Konsep abstrak yang merepresentasikan interaksi antara browser dan server disebut', 'Stateless', 'Statefull', 'Session', 'cookies ', 'HTTP', 'C', '12321'),
(35, 2, 'Web Stateless umumnya memngunakan protokol', ' HTTP', 'HTTPS', 'Stateless', 'WWW', 'Semua Jawaban salah', 'A', '12321'),
(36, 3, ' Aplikasi web dimana antara suatu interaksi request-response dengan request-response lainnya bersifat independen, tidak memiliki keterkaitan satu sama lain. Disebut dengan', 'Stateless', 'Statefull', 'Sessioon', 'Cookies', ' HTTP', 'A', '12321'),
(37, 4, 'Sistem mengetahui keadaan user sekarang dan dapat “melanjutkan” halaman apa yang diminta oleh user daripada dia memberikan apa yang diminta user dengan langsung memberikan halaman baru, merupakan sebuah keadaan yang disebut', 'Stateless', 'Statefull', 'Sessioon', 'Cookies', 'HTTP', 'A', '12321'),
(38, 5, 'File kecil yang diletakkan oleh server pada komputer pengguna disebut', 'Stateless', 'Statefull', 'Stateting', 'Cookies', 'Protokol', 'D', '12321'),
(39, 6, 'Fakta mengenai cookies yang benar dibawah ini adalah', 'Cookies seperti virus yang bisa menghapus data di komputer kita', 'Cookies bisa digunakan untuk melacak kebiasaan kita dalam melihat suatu situs', 'Cookies bisa mencuri informasi kita', 'Cookies digunakan untuk spam', 'Cookies sangat berbahaya', 'B', '12321'),
(40, 7, 'Tipe default, cookie sementara yang hanya disimpan di memory browser, termasuk kedalam jenis cookies', 'Session Cookies', 'Default Cookies', ' Persistent Cookies', ' Browser Cookies', 'Super Cookies', 'A', '12321'),
(41, 8, 'Cookies yang disimpan pada sebuah file yang ada di komputer browser. Disebut dengan', 'Session Cookies', 'Default Cookies', 'Persistent Cookies', '. Browser Cookies', 'Super Cookies', 'C', '12321'),
(42, 9, 'Sintaks yang digunakan sebagai menandakan server menginginkan untuk memulai sesi dengan user adalah', 'Session_user()', 'Session_start()', 'Session_begin()', 'Session_cookies()', 'Session_inset()', 'B', '12321'),
(43, 10, ' Session bisa dihapus secara paksa dengan perintah biasanya dilakukan saat user logout dengan cara sintaks', 'Session_unisset() ', 'Session_close() ', 'session_destroy()', 'Session_delete() ', 'Session_end()', 'C', '12321'),
(44, 11, '2 macam penyandian data dalam website adalah', 'Encryption dan Descryption', 'Encryption dan Hashing', 'Session dan Cookies', 'Descryption dan Hashing', 'HTTP dan HTTPS', 'C', '12321'),
(45, 12, 'Menanggulangi penyadapan telepon dan email adalah manfaat dati penggunaan', 'Hashing', 'Descryption', 'HTTPS', 'Firewall', 'Encryption', 'E', '12321'),
(46, 13, 'Rangkaian aksara yang dapat terdiri atas huruf (A-Z), angka(0-9), tanda baca, atau lambang matematika adalah maksud dari', 'Numeric', 'Hashing', 'Validasi', 'Alfanumeric', 'Encryption', 'D', '12321'),
(47, 14, 'Suatu tindakan yang membuktikan bahwa suatu proses/metode dapat memberikan hasil yang konsisten sesuai dengan spesifikasi yang telah ditetapkan dan terdokumentasi dengan baik adalah pengertian dari', 'Numeric', 'Hashing', 'Validasi', 'Alfanumeric', 'Encryption', 'C', '12321'),
(48, 15, 'Sebuah fungsi yang digunakan untuk mengacak sebuah kata menjadi kata lain yang tidak bermakna dan sedapat mungkin kata hasil hashing tidak bisa ditebak dari kata apa kata tersebut berasal disebut juga', 'Hashing', 'Descryption', 'HTTPS', 'Firewall', 'Encryption', 'A', '12321'),
(49, 16, 'Pesan tidak bisa dibaca bila penerima pesan lupa atau kehilangan kunci (decryptor) adalah kerugian dari pemakaian', 'Hashing', 'Descryption', 'HTTPS', 'Firewall', 'Encryption', 'E', '12321'),
(50, 17, 'Yang tidak termasuk metode encryption adalah', 'MD 2', 'MD 4', 'MD 5', 'Base64', 'Base32', 'E', '12321'),
(51, 1, 'Apa yang di namakan session', 'Stateless', 'Statefull', 'cookies adalah loremipsum', 'Cookies', 'Encryption', 'C', 'bcd1u'),
(52, 22, 'Apa yang di namakan session', 'Stateless', 'Descryption', 'Sessioon', 'Cookies', 'Encryption', 'B', 'bcd1u'),
(53, 3, 'Apa yang di namakan session', 'Stateless', 'Statefull', 'djdjd', 'Cookies', 'Encryption', 'A', 'bcd1u'),
(54, 4, 'Apa yang di namakan session', 'Hashing', 'Descryption', 'Sessioon', 'Cookies', 'Encryption', 'B', 'bcd1u');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `no_soal` int(11) NOT NULL,
  `kode_soal` varchar(255) NOT NULL,
  `jawaban` varchar(255) NOT NULL,
  `value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id`, `nis`, `id_soal`, `no_soal`, `kode_soal`, `jawaban`, `value`) VALUES
(52, 15673, 30, 1, '89jhu', 'D', NULL),
(53, 15673, 31, 2, '89jhu', 'C', NULL),
(54, 15673, 32, 3, '89jhu', 'C', NULL),
(55, 15673, 33, 4, '89jhu', 'C', NULL),
(60, 15646, 33, 4, '89jhu', 'B', NULL),
(61, 15646, 30, 1, '89jhu', 'D', NULL),
(62, 15673, 54, 4, 'bcd1u', 'B', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `list_soal`
--

CREATE TABLE `list_soal` (
  `id` int(11) NOT NULL,
  `guru_mapel` varchar(255) NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `deskripsi_ulangan` varchar(255) DEFAULT NULL,
  `waktu` varchar(255) NOT NULL,
  `kode_soal` varchar(255) NOT NULL,
  `creator` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_soal`
--

INSERT INTO `list_soal` (`id`, `guru_mapel`, `mapel`, `deskripsi_ulangan`, `waktu`, `kode_soal`, `creator`) VALUES
(10, 'Sukonto Legowo', 'ppkn', 'Ulangan ke-3', '60 menit', 'bcd1u', '54321'),
(11, 'Nama Guru', 'Sejarah', 'ulangan susulan', '45 Menit', '89jhu', '12345'),
(12, 'Adi Setiawan', 'Pemrogaman Web', 'ulangan bab 1', '60 menit', '12321', '15679'),
(14, 'Sukonto Legowo', 'Pemdas', 'ulangan bab 3', '60 menit', 'asdf', '54321');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `no_abs` int(11) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nis`, `password`, `nama`, `no_abs`, `kelas`, `tanggal_lahir`, `jenis_kelamin`, `role_id`) VALUES
(4, 15673, '$2y$10$jj2f2Q3eBe4waHBhM5FuFeA9kxP/V.qrpVtA9fu7eouCILUJwwt9S', 'Roni Angga', 2, 'XII RPL 2', '2002-08-15', 'Laki-laki', 1),
(6, 12345, '$2y$10$9vQmVUHIKAZxX3IJJ3w7ROlR2oqUgZ.PPxMCaO7SLcpD03fuQ.lKG', 'Nama Guru', 0, 'Guru', '2002-04-02', 'Laki-laki', 2),
(7, 54321, '$2y$10$uJUO.Xqf4wWcvPlsF8SPUuRLx/FMYTBBYyDQSIGFNbG4ou.wixUpq', 'Sukonto Legowo', 0, 'Guru', '2002-04-12', 'Perempuan', 2),
(11, 15674, '$2y$10$f4SHIaViEETkAg5zG6JKM.LTZdUKPIIpFSbglhub.IKHbZeb4HlMe', 'Alif handoyo', 2, 'XII RPL 2', '2019-09-06', 'Laki-laki', 1),
(12, 15675, '$2y$10$ZTY886qPHsefv5/hicZ0FuqS6N4AHZIh68UrKAeNCEFSN/wdoegE6', 'Aflah Eka Satya', 3, 'XII RPL 2', '2019-09-06', 'Laki-laki', 1),
(13, 15676, '$2y$10$w84kcC376tqTAl6I2UHEf.Zcf9B7mIlv62KKAKKlmAUCspJw0tD2C', 'Herdino Yanuar', 4, 'XII RPL 2', '2019-09-05', 'Laki-laki', 1),
(14, 15677, '$2y$10$48MIzzWvN9H44c9szi8S3.35HsBp7lGIn09GOUDknIJKXZOOdFfT2', 'Budianto', 44, '44', '2019-10-15', 'Laki-laki', 1),
(15, 15641, '$2y$10$mh83pbKEX3SjVDq6Ztg78emSq4XvwvbAcbqJfOK4hLCvEY/bK3uYK', 'Rangga ', 1, 'XII RPL 2', '2003-03-22', 'Laki-laki', 1),
(16, 15642, '$2y$10$QtLWcMFlcPOTqhwZ6qia1eMOBrEj2n1AOjkhfN8JhxazR93PrFxpC', 'Bagas', 5, 'XII RPL 2', '2019-10-01', 'Laki-laki', 1),
(17, 15679, '$2y$10$Th4CHS2nvsC/0szNot8v6Otg.WUWet6iiOL63A/SvlvnxP.4otHHu', 'Adi Setiawan', 45, 'guru', '2019-10-08', 'Laki-laki', 2),
(18, 15666, '$2y$10$9U81Zc05os8DVH4nDBcHUupuaITi..Kmko8L0ijW3zuUuXjccGkr6', 'biostaria sayang hewan', 69, 'XII RPL 2', '2019-10-30', 'Perempuan', 1),
(19, 156777, '$2y$10$gAowFhwzfyB9ZesMH891Y.PSMexBqqZTCE7XphS1MwaHqBkb7ovEO', 'Roni angga', 55, 'XII RPL 2', '2019-10-07', 'Laki-laki', 1),
(20, 15646, '$2y$10$usi2bx3/1WdBauXuj5.zVO9ftvFy6rw6T7BUFVeHL5la4XvL3ZjHe', 'biostaria sayang hewan', 7, 'XII RPL 2', '2019-10-07', 'Laki-laki', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank_soal`
--
ALTER TABLE `bank_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_soal`
--
ALTER TABLE `list_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank_soal`
--
ALTER TABLE `bank_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `list_soal`
--
ALTER TABLE `list_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
